import 'package:flutter/material.dart';

class RecommenedMenu extends StatelessWidget {
  const RecommenedMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[100],
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 50,bottom: 10),
            child: Image.asset("assets/images/recom.png",height: 180,),
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/กุ้งอบวุ้นเส้น.jpg"),
              radius: 30,
            ),
            title: Text(
              'Glass Noodle Hot Pot',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '200 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.favorite,
              size: 25,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("กุ้งอบวุ้นเส้น",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เมนูสไตล์ไทยปนจีนที่เป็นที่นิยม ความอร่อยของวุ้นเส้นมาจากเครื่องหอมต่างๆ มากมายที่ใส่ลงไปอบในหม้อ พร้อมกับการปรุงรสอย่างง่ายๆ กุ้งที่ใช้อบควรมีเปลือก ทำให้เนื้อกุ้งไม่แข็งกระด้าง และเปลือกกุ้งก็ช่วยเพิ่มความหอมด้วยเช่นกัน"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.amber),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/ต้มแซ่บกระดูกอ่อน.jpg"),
              radius: 30,
            ),
            title: Text(
              'Fiery Pork Ribs Broth',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '120 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.favorite,
              size: 25,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("ต้มแซ่บกระดูกอ่อน",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("อาหารจานเด็ดสุดแซ่บ กับกระดูกหมู ต้มจนนุ่มเปื่อยยุ่ย เติมเต็มความอร่อยด้วยสมุนไพรหลายชนิด ทั้งตะไคร้ ใบมะกรูด หอมแดง ผักชีฝรั่ง ได้ทั้งความเผ็ดร้อนและกลิ่นหอม ที่ชวนให้เข้าไปลิ้มลองสุด ๆ ซดได้คล่องคอไม่รู้จักเบื่อ"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.amber),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/ปูผัดผงกะหรี่.jpg"),
              radius: 30,
            ),
            title: Text(
              'Curry Crab',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '550 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.favorite,
              size: 25,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("ปูผัดผงกะหรี่",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เป็นอาหารไทยที่ปรุงรสด้วยเครื่องปรุงจากอินเดีย มีรสชาติเข้มข้นเป็นเอกลักษณ์ หอมกลิ่นเครื่องเทศ รสจัด  แต่เมื่อทานกับเนื้อปูแล้ว อร่อยกลมกล่อมเป็นที่สุด"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.amber),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/ผัดไท.jpg"),
              radius: 30,
            ),
            title: Text(
              'Pad Thai',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '80 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.favorite,
              size: 25,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("ผัดไท",style: TextStyle(fontWeight: FontWeight.bold),),
                    content: new Text("เป็นเมนูจานเส้นสไตล์ไทยที่ได้รับอิทธิพลจากอาหารจีน ปัจจุบันถือเป็นหนึ่งในอาหารประจำชาติไทย รสชาติหลักของผัดไทยมาจากซอสที่เคี่ยวจากน้ำตาล น้ำปลา และมะขามเปียก จุดเด่นของผัดไทยอยู่ที่เส้นจันท์ที่มีลักษณะคล้ายเส้นเล็ก แต่จะเหนียวกว่า"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.amber),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/ยำปลาดุกฟู.jpg"),
              radius: 30,
            ),
            title: Text(
              'Crispy Catfish Salad',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '100 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.favorite,
              size: 25,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("ยำปลาดุกฟู",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เนื้อปลาดุกย่างยีจนฟูแล้วนำไปทอดในน้ำมันร้อนๆ รับประทานกับยำมะม่วง เคล็ดลับในการทอดปลาดุกให้ฟูกรอบอยู่ที่ความร้อนของน้ำมันและความแห้งของเนื้อปลาดุกก่อนที่จะนำไปทอด"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.amber),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/เมี่ยงคำ.jpg"),
              radius: 30,
            ),
            title: Text(
              'Miang kham',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              '150 Bath',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.favorite,
              size: 25,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: new Text("เมี่ยงคำ",style: TextStyle(fontWeight: FontWeight.bold)),
                    content: new Text("เป็นของว่างที่คนภาคกลางนิยมรับประทานในช่วงฤดูฝน เนื่องจากเป็นช่วงที่ต้นชะพลูผลิใบแตกยอดอ่อนมากที่สุด จัดเป็นสำรับของว่างรสชาติดีทีเดียว"),
                    actions: [
                      Visibility(
                        visible: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10,right: 10),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.amber),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10,left: 140,right: 140),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.amber),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ))),
                child: Text(
                  "Back",
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
                onPressed: () {
                  Navigator.pop(context,"...");
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
