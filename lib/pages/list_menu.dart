import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqlite_basic/database/database_menu.dart';
import 'package:sqlite_basic/model/menu_model.dart';
import 'package:sqlite_basic/pages/add_menu.dart';
import 'package:sqlite_basic/pages/edit_menu.dart';
import 'package:sqlite_basic/pages/show_menu.dart';

class ListMenu extends StatefulWidget {
  const ListMenu({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ListMenu> createState() => _ListMenuState();
}

class _ListMenuState extends State<ListMenu> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[200],
      appBar: AppBar(
        title: Text(
          "Menu",
          style: TextStyle(fontSize: 22),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add),
              tooltip: 'Add Menu',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddMenu()),
                ).then((value) {
                  //getAllData();
                  setState(() {});
                });
              },
            ),
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<MenuModel>>(
            future: DatabaseMenu.instance.getProfiles(),
            builder: (BuildContext context,
                AsyncSnapshot<List<MenuModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Menus in List.'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Center(
                          child: Card(
                            color: selectedId == grocery.id
                                ? Colors.white70
                                : Colors.amber[100],
                            child: ListTile(
                              title: Text(
                                '${grocery.name}',
                                style: TextStyle(fontSize: 18),
                              ),
                              subtitle: Text(
                                '${grocery.price} Bath',
                                style: TextStyle(fontSize: 15),
                              ),
                              leading: CircleAvatar(
                                backgroundImage: FileImage(File(grocery.image)),
                                radius: 30,
                              ),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditMenu(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "Do you want to delete this record?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseMenu.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShowMenu(id: profileid)));

                                setState(() {
                                  print(grocery.image);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  DatabaseMenu.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
