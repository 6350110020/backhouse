import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sqlite_basic/database/database_menu.dart';
import 'package:sqlite_basic/model/menu_model.dart';
import 'dart:developer' as developer;

class EditMenu extends StatefulWidget {
  MenuModel model;
  EditMenu(this.model);

  @override
  _EditMenuState createState() => _EditMenuState(model);
}

class _EditMenuState extends State<EditMenu> {
  var name = TextEditingController();
  var price = TextEditingController();
  var quantity = TextEditingController();

  MenuModel model;
  int? selectedId;

  var _image;
  var imagePicker;
  _EditMenuState(this.model);

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    this.selectedId = this.model.id;
    name  = TextEditingController()..text = this.model.name;
    price = TextEditingController()..text = this.model.price;
    quantity = TextEditingController()..text = this.model.quantity;
    _image = this.model.image;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Menu'),
      ),
      body: Container(
        color: Colors.amber[100],
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                         final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              preferredCameraDevice: CameraDevice.front);

                          setState(() {
                            _image = image.path;
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                              BoxDecoration(color: Colors.amber[300]),
                          child: _image != null
                              ? Image.file(
                                  File(_image),
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.amber[300]),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    buildTextfield("name", name),
                    buildTextfield("price", price),
                    buildTextfield("quantity", quantity),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Update'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Update') {
              await DatabaseMenu.instance.update(
                  MenuModel(
                      id: selectedId,
                      name: name.text,
                      price: price.text,
                      quantity: quantity.text,
                      image: _image),
                );
          setState(() {
            //firstname.clear();
            selectedId = null;
          });
          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          fixedSize: Size(120, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

}
