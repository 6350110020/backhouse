import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sqlite_basic/homepage.dart';
import 'package:sqlite_basic/pages/list_menu.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Menu',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: const Homepage(title: "homepage"),
      debugShowCheckedModeBanner: false,
    );
  }
}



