import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:sqlite_basic/pages/about.dart';
import 'package:sqlite_basic/pages/list_menu.dart';
import 'package:sqlite_basic/pages/recommened_menu.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key, required String title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[100],
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 40, top: 80),
              child: Image.asset("assets/images/logo.png", width: 280),
            ),
            SizedBox(
              width: 250,
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.amber[300]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ))),
                child: Text(
                  "Menu",
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context){
                        return ListMenu(title: 'Profile Lists');
                      })
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: SizedBox(
                width: 250,
                height: 60,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.amber),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ))),
                  child: Text(
                    "Recommened Menu",
                    style: TextStyle(fontSize: 22, color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return RecommenedMenu();
                        })
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: SizedBox(
                width: 250,
                height: 60,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                      MaterialStateProperty.all(Colors.amber[700]),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ))),
                  child: Text(
                    "About",
                    style: TextStyle(fontSize: 22, color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return About();
                        })
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 80, top: 30),
              child: Row(
                children: [
                  Icon(
                    Icons.local_restaurant,
                    color: Colors.black,
                    size: 30,
                  ),
                  Text(
                    " :  PSU TRANG",
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 80, top: 5),
              child: Row(
                children: [
                  Icon(
                    Icons.phone,
                    color: Colors.black,
                    size: 30,
                  ),
                  Text(
                    " :  089-958-9993",
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
